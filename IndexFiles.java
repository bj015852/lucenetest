/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lucene.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DecimalFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.search.postingshighlight.PostingsHighlighter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.AttributeSource;
import org.apache.lucene.util.BytesRef;

/**
 * Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing. Run
 * it with no command-line arguments for usage information.
 */
public class IndexFiles {

	private IndexFiles() {
	}

	/** Index all text files under a directory. */
	public static void main(String[] args) {
		String indexPath = "C:\\index";
		String docsPath = "C:\\docs";
		String spellPath = "C:\\index\\spell\\";
		boolean create = false;

		final Path docDir = Paths.get(docsPath);
		if (!Files.isReadable(docDir)) {
			System.out.println("Document directory '" + docDir.toAbsolutePath()
					+ "' does not exist or is not readable, please check the path");
			System.exit(1);
		}

		Date start = new Date();

		try {
			System.out.println("Indexing to directory '" + indexPath + "'...");

			Directory dir = FSDirectory.open(Paths.get(indexPath));

			Analyzer analyzer = new StandardAnalyzer();
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

			if (create) {
				// Create a new index in the directory, removing any
				// previously indexed documents:
				iwc.setOpenMode(OpenMode.CREATE);
			} else {
				// Add new documents to an existing index:
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}

			// Optional: for better indexing performance, if you
			// are indexing many documents, increase the RAM
			// buffer. But if you do this, increase the max heap
			// size to the JVM (eg add -Xmx512m or -Xmx1g):
			//
			// iwc.setRAMBufferSizeMB(256.0);

			IndexWriter writer = new IndexWriter(dir, iwc);
			indexDocs(writer, docDir);

			// NOTE: if you want to maximize search performance,
			// you can optionally call forceMerge here. This can be
			// a terribly costly operation, so generally it's only
			// worth it when your index is relatively static (ie
			// you're done adding documents to it):
			//
			// writer.forceMerge(1);

			System.out.println("Docs Number: " + writer.numDocs());
			writer.close();

			Date end = new Date();
			System.out.println(end.getTime() - start.getTime() + " total milliseconds");

		} catch (IOException e) {
			System.out.println(" caught a " + e.getClass() + "\n with message: " + e.getMessage());
		}

		try {
			Directory dir = FSDirectory.open(Paths.get(indexPath));

			Analyzer analyzer = new StandardAnalyzer();
			IndexReader reader = DirectoryReader.open(dir);
			IndexSearcher searcher = new IndexSearcher(reader);

			String qStr1 = "water";
			QueryParser queryParser = new QueryParser("contents", analyzer);
			Query query = queryParser.parse(qStr1);
			TopDocs topDocs = searcher.search(query, 10);
			System.out.println("Hit " + qStr1 + ": " + topDocs.totalHits);
			outputSearchResultDetail(topDocs, reader, query, qStr1);

			qStr1 = "study";
			query = queryParser.parse(qStr1);
			topDocs = searcher.search(query, 10);
			System.out.println("Hit " + qStr1 + ": " + topDocs.totalHits);
			outputSearchResultDetail(topDocs, reader, query, qStr1);
			

			
		} catch (IOException e) {
			System.out.println("Error when read the index!");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Parse Err!");
		}
		System.out.println("Done");
	}

	/**
	 * Indexes the given file using the given writer, or if a directory is
	 * given, recurses over files and directories found under the given
	 * directory.
	 * 
	 * NOTE: This method indexes one document per input file. This is slow. For
	 * good throughput, put multiple documents into your input file(s). An
	 * example of this is in the benchmark module, which can create "line doc"
	 * files, one document per line, using the <a href=
	 * "../../../../../contrib-benchmark/org/apache/lucene/benchmark/byTask/tasks/WriteLineDocTask.html"
	 * >WriteLineDocTask</a>.
	 * 
	 * @param writer
	 *            Writer to the index where the given file/dir info will be
	 *            stored
	 * @param path
	 *            The file to index, or the directory to recurse into to find
	 *            files to index
	 * @throws IOException
	 *             If there is a low-level I/O error
	 */
	static void indexDocs(final IndexWriter writer, Path path) throws IOException {
		if (Files.isDirectory(path)) {
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					try {
						indexDoc(writer, file, attrs.lastModifiedTime().toMillis());
					} catch (IOException ignore) {
						// don't index files that can't be read.
					}
					return FileVisitResult.CONTINUE;
				}
			});
		} else {
			indexDoc(writer, path, Files.getLastModifiedTime(path).toMillis());
		}
	}

	/** Indexes a single document */
	static void indexDoc(IndexWriter writer, Path file, long lastModified) throws IOException {
		try (InputStream stream = Files.newInputStream(file)) {
			// make a new, empty document
			Document doc = new Document();

			doc.add(new StringField("title", file.getFileName().toString(), Field.Store.YES));

			// Add the path of the file as a field named "path". Use a
			// field that is indexed (i.e. searchable), but don't tokenize
			// the field into separate words and don't index term frequency
			// or positional information:
			Field pathField = new StringField("path", file.toString(), Field.Store.YES);
			doc.add(pathField);

			// Add the last modified date of the file a field named "modified".
			// Use a LongPoint that is indexed (i.e. efficiently filterable with
			// PointRangeQuery). This indexes to milli-second resolution, which
			// is often too fine. You could instead create a number based on
			// year/month/day/hour/minutes/seconds, down the resolution you
			// require.
			// For example the long value 2011021714 would mean
			// February 17, 2011, 2-3 PM.
			doc.add(new LongPoint("modified", lastModified));

			// Add the contents of the file to a field named "contents". Specify
			// a Reader,
			// so that the text of the file is tokenized and indexed, but not
			// stored.
			// Note that FileReader expects the file to be in UTF-8 encoding.
			// If that's not the case searching for special characters will
			// fail.
			FieldType contentsType = new FieldType();
			contentsType.setStored(false);
			contentsType.setOmitNorms(true);
			contentsType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
			contentsType.setStoreTermVectors(true);
			contentsType.setStoreTermVectorPositions(true);
			contentsType.setStoreTermVectorOffsets(true);

			contentsType.freeze();
			// new Field("contents", "value", contentsType);
			doc.add(new Field("contents", new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8)),
					contentsType));

			if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
				// New index, so we just add the document (no old document can
				// be there):
				System.out.println("adding " + file);
				writer.addDocument(doc);
			} else {
				// Existing index (an old copy of this document may have been
				// indexed) so
				// we use updateDocument instead to replace the old one matching
				// the exact
				// path, if present:
				System.out.println("updating " + file);
				writer.updateDocument(new Term("path", file.toString()), doc);
			}
		}
	}

	static void outputSearchResultDetail(TopDocs topDocs, IndexReader reader, Query query, String qStr) {
		System.out.println(StringUtils.rightPad("Title", 40) + StringUtils.rightPad("Path", 20)
				+ StringUtils.center("id", 4) + StringUtils.center("score", 15) + StringUtils.center("freq", 15)
				+ StringUtils.rightPad("contents", 100));
		DecimalFormat scoreFormatter = new DecimalFormat("0.######");
		try {
			for (ScoreDoc match : topDocs.scoreDocs) {
				Document doc = reader.document(match.doc);

				Terms terms = reader.getTermVector(match.doc, "contents");
				// System.out.println(terms.hasFreqs());
				// System.out.println(terms.hasPositions());
				// System.out.println(terms.hasOffsets());
				TermsEnum termsEnum = terms.iterator();
				BytesRef termBytes = null;
				int freq = 0;
				while ((termBytes = termsEnum.next()) != null) {
					String term = termBytes.utf8ToString();
					if (term.equals(qStr)) {
						freq = (int) termsEnum.totalTermFreq();
						break;
					}
				}

				String bestFrag = getHighlightFrag_DefaultHighlighter(match, reader, query, qStr);

				System.out.println(StringUtils.rightPad(StringUtils.abbreviate(doc.get("title"), 39), 40)
						+ StringUtils.rightPad(StringUtils.abbreviate(doc.get("path"), 19), 20)
						+ StringUtils.center("" + match.doc, 4)
						+ StringUtils.center(scoreFormatter.format(match.score), 15)
						+ StringUtils.center(scoreFormatter.format(freq), 15)
						+ StringUtils.rightPad(StringUtils.abbreviate(bestFrag, 99), 100));
				
				
			}
		} catch (IOException e) {
			System.out.println("Error when read the index!");
		}
	}

	/** Default Highlighter for contents field relies on TermVector stored in the Index.
	 *  Actually the default Highlight could highlight by means of either <B>reanalyzing</B> or 
	 *  getting <B>TokenStream</B> from TermVector. Here we don't reanalyze the contents, 
	 *  just use the TokenStream.
	 * 
	 * @param match     Matched Document from TopDocs
	 * @param reader	IndexReader
	 * @param query		Query
	 * @param qStr		Query input string
	 * @return			contents fragments with Highlight tags
	 */
	static String getHighlightFrag_DefaultHighlighter(ScoreDoc match, IndexReader reader, Query query, String qStr) {
		QueryScorer scorer = new QueryScorer(query, reader, "contents");
		Fragmenter fragmenter = new SimpleSpanFragmenter(scorer);
		Highlighter highlighter = new Highlighter(scorer);
		highlighter.setTextFragmenter(fragmenter);
		
		try {
			Document doc = reader.document(match.doc);
			String pathStr = doc.getField("path").stringValue();
			File file = new File(pathStr);
			String contentStr = FileUtils.readFileToString(file, Charset.forName("UTF-8"));

			TokenStream tokenStream = TokenSources.getTermVectorTokenStreamOrNull("contents",
					reader.getTermVectors(match.doc), -1);
			
			String bestFrag = highlighter.getBestFragment(tokenStream, contentStr);
			
			return bestFrag;
		} catch (IOException e) {
			System.out.println("Error when read the index!");
		} catch (InvalidTokenOffsetsException e) {
			System.out.println("Check TermVector and TokenStream!");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Postings Highlighter return a array of highlighted results. It uses
	 * positions and offsets stored in the postings list, which is a new
	 * capability in Lucene 4.0. It needs the fields being stored.
	 * 
	 * @param Field field name string. Must have a stored string value and also be indexed with offsets.
	 * @param query
	 * @param searcher
	 * @param topDocs TopDocs containing the summary result documents to highlight.
	 * @return Array of formatted snippets corresponding to the documents in
	 *         topDocs. If no highlights were found for a document, the first
	 *         sentence for the field will be returned.
	 */
	static String[] getHighLightFrag_PostingsHighlighter(String field, Query query, IndexSearcher searcher, TopDocs topDocs){
		PostingsHighlighter highlighter = new PostingsHighlighter();
		try {
			String[] frags = highlighter.highlight(field, query, searcher, topDocs);
			return frags;
		} catch (IOException e) {
			System.out.println("Error when read the index!");
			return null;
		}
	}
	
}
